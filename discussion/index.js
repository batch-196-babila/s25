// JS Object Notation - data format for app. to communicate with each other
// JSON may look like JS object but it's actually a String

/*
    JSON Syntax:
    {
        "ket1": "valueA",
        "ket2": "valueB"
    }
    keys are wrapped in {}
*/

let sample1 = `{
    "name": "Katniss Everdeen",
    "age": 20,
    "address": {
        "city": "Kansas City",
        "state": "Kansas"
    }
}`;

console.log(sample1);

// JSON into JS Object using JSON.parse()
console.log(JSON.parse(sample1));

// JSON Array

let sampleArr = `[
    {
        "email": "jasonNewsted@gmail.com",
        "password": "iplaybass1234",
        "isAdmin": false
    },
    {
        "email": "larsDrum@gmail.com",
        "password": "metallicaMe80",
        "isAdmin": true
    }
]`;
console.log(sampleArr);
console.log(JSON.parse(sampleArr));
// we can't used Array methods on JSON array bec it's a string

// we need to convert first
// parsed the JSON array to JS array
let parseSampleArr = JSON.parse(sampleArr);
console.log(parseSampleArr);

// deleteling last item
console.log(parseSampleArr.pop());

// JSON.parse() does not mutate or update the original JSON
// Sending data back to client/frontend, it should be in JSON format
// We can turn JS Object into a JSON
// JSON.stringify() - will stringify JS objects as JSON

sampleArr = JSON.stringify(parseSampleArr);
console.log(sampleArr);

// API
// Database (JSON) => Server/API (JSON to JS Object) => sent as JSON => client/frontend

/* Mini Act 
    Process given JSON array and convert to JS Object
    delete last item and add new item in JSON array
    Stringify the array back in JSON
    Update jsonArr with stringified array
*/

let jsonArr = `[
    "pizza",
    "hamburger",
    "spaghetti",
    "shanghai",
    "hotdog stick on a pineapple",
    "pancit bihon"
]`;

// convert JSON array to JS array
let convertedJsonArr = JSON.parse(jsonArr);


convertedJsonArr.pop();
convertedJsonArr.push("cupcake");

// Update to become JSON again

jsonArr = JSON.stringify(convertedJsonArr);
console.log(jsonArr);

